/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2016/3/18 15:18:11                           */
/*==============================================================*/


drop table if exists Singer;

/*==============================================================*/
/* Table: Singer                                                */
/*==============================================================*/
create table Singer
(
   sin_id               int not null auto_increment,
   sin_portrait         varchar(255),
   sin_nickname         char(15),
   sin_gender           char(1),
   sin_gendercheck      int(1),
   sin_username         varchar(20),
   sin_password         varchar(25),
   sin_email            varchar(50),
   sin_emailcheck       int(1),
   sin_sina             varchar(50),
   sin_sinacheck        int(1),
   sin_weixin           varchar(50),
   sin_weixincheck      int(1),
   sin_qq               varchar(20),
   sin_qqcheck          int(1),
   sin_follow           int,
   sin_fans             int,
   sin_visitor          int,
   sin_location         text,
   sin_synopsis         text,
   primary key (sin_id)
);

alter table Singer comment 'Singer Table';

